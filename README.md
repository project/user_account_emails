CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation

INTRODUCTION
------------

This module allows administrators to send user account emails, for example, a password reset email, to other user
accounts. The emails will be sent to the email registered with the selected account.


INSTALLATION
------------

The installation of this module is like other Drupal modules.

 1. If your site is [managed via Composer](https://www.drupal.org/node/2718229),
    use Composer to download the webform module running
    ```composer require "drupal/user_account_emails"```. Otherwise copy/upload the
    module to the modules directory of your Drupal installation.

 2. Enable the module in 'Extend'.
   (/admin/modules)

 3. Set up user permissions. (/admin/people/permissions#user_account_emails)
