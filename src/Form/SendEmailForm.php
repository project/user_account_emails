<?php

namespace Drupal\user_account_emails\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\UserInterface;

/**
 * Provides a User account emails form.
 */
class SendEmailForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'user_account_emails_send_email';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, UserInterface $user = NULL) {
    // Build the options, depending on whether the user is active.
    if (!$user->isActive()) {
      $options = [
        'register_pending_approval' => $this->t('Welcome (awaiting approval)'),
        'status_blocked' => $this->t('Account blocked'),
      ];
      \Drupal::messenger()->addMessage('This account is blocked or not activated, so some of the options are not available.');
    }
    else {
      $options = [
        'password_reset' => $this->t('Password recovery'),
        'register_no_approval_required' => $this->t('Welcome (no approval required)'),
        'status_activated' => $this->t('Account activation'),
      ];
    }

    $form['message_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Message type'),
      '#description' => $this->t('Choose which type of message to send.'),
      '#options' => $options,
      '#required' => TRUE,
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Send'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\user\UserInterface $account */
    $account = \Drupal::routeMatch()->getParameter('user');
    $op = $form_state->getValue('message_type');
    $mail = _user_mail_notify($op, $account);
    if (empty($mail)) {
      \Drupal::messenger()->addError('Unable to send the email');
    }
    else {
      \Drupal::messenger()->addMessage('Email sent successfully');
      $form_state->setRedirect('entity.user.canonical', ['user' => $account->id()]);
    }
  }

}
